#!/usr/bin/env node

const path = require('path');
const fs = require('fs');

const inquirer = require('inquirer');
const glob = require('glob');
const ProgressBar = require('progress');
const request = require('superagent');

require('superagent-proxy')(request);

const ARGS = require('minimist')(process.argv.slice(2));

let CONFIGFILE;
if (ARGS.config) {
  CONFIGFILE = ARGS.config;
} else if (fs.existsSync('./fsmuploadconfig.local.js')) {
  CONFIGFILE = './fsmuploadconfig.local.js';
} else if (fs.existsSync('./fsmuploadconfig.js')) {
  CONFIGFILE = './fsmuploadconfig.js';
}

const CONFIG = require(path.join(process.cwd(), CONFIGFILE));
Object.assign(CONFIG, ARGS);

let ZIPS = []
const questions = [];

if (!ARGS.allzips) {
  if (!CONFIG.glob) {
    throw new Error('Glob parameter is required when not uploading all zips')
  }

  ZIPS = glob.sync(CONFIG.glob);

  questions.push({
    type: 'list',
    name: 'zip',
    message: 'Zip file:',
    choices: ZIPS,
  })
}

let environment;
if (!ARGS.environment) {
  questions.push({
    type: 'list',
    name: 'environment',
    message: 'Upload to environment:',
    choices: CONFIG.environments,
  });
} else {
  environment = CONFIG.environments.find(x => x.name === ARGS.environment).value;
}

inquirer.prompt(questions)
  .then((answers) => {
    console.log('Freestyle designzip uploader');

    environment = answers.environment || environment;

    // Default all found zips
    uploadZips = ZIPS

    // When single zip is selected
    if (answers.zip) {
      uploadZips = [answers.zip]
    }

    // If no zips found, check environment specific zips
    if (!uploadZips.length) {
      if (!environment.zips) {
        throw new Error('Environment is missing zips')
      }

      environment.zips.forEach((zipPattern) => {
        glob.sync(zipPattern).forEach(zip => uploadZips.push(zip))
      })
    }

    if (CONFIG.proxy) {
      console.log('Using proxy', CONFIG.proxy);
    }

    const zipQue = [...uploadZips];
    let failedCount = 0;
    const uploadingZips = [];
    const uploadLimit = environment.concurrentRequest || 1;

    const dest = `${environment.url}/web/freestyledesignarchiveupload`;
    const uploadZip = (zip) => {
      const req = request.post(dest);

      if (CONFIG.proxy) {
        req.proxy(CONFIG.proxy);
      }

      if (environment.key) {
        req.set('AuthViaApplicationKey', environment.key);
      }

      if (environment.auth) {
        req.auth(environment.auth.user, environment.auth.password);
      }

      console.log('Sending request to', environment.url);

      let bar;
      let last = 0;

      return req
        .attach('file1', `./${zip}`)
        .on('progress', e => {
          if (!bar) {
            const total = e.total;
            bar = new ProgressBar(`${zip}: [:bar]`, { total });
          } else {
            bar.tick(e.loaded - last);
            last = e.loaded;
          }
        })
        .then((res) => {
          console.log(res.status, zip);
        });
    }

    const processQue = () => {
      if (zipQue.length === 0 || uploadingZips.length >= uploadLimit) {
        return Promise.resolve();
      }

      const zip = zipQue.shift();
      uploadingZips.push(zip);
      return uploadZip(zip)
        .catch(err => {
          console.error('Upload failed for', zip, err);
          failedCount++;
        })
        .finally(() => {
          const index = uploadingZips.indexOf(zip);
          if (index !== -1) {
            uploadingZips.splice(index, 1);
          }
        });
    }

    const startUpload = () => {
      return new Promise((resolve, reject) => {
        const intervalId = setInterval(() => {
          if (zipQue.length === 0 && uploadingZips.length === 0) {
            clearInterval(intervalId);
            resolve({ failedCount });
            return;
          }

          processQue().catch(err => {
            clearInterval(intervalId);
            reject(err);
          });
        }, 250);
      });
    }

    return startUpload();
  })
  .then(({ failedCount }) => {
    if (failedCount !== 0) {
      throw new Error(`${failedCount} zip(s) failed to upload`);
    }
  })
  .catch(err => {
    console.log(err);
    process.exit(1);
  });
